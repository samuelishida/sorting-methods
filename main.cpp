#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <iomanip>

using namespace std;

void swap(int& a, int& b);
void merge(int arr[], int l, int m, int r, int buffer[]);

// function prototypes for sorting methods
void shellSort(int* vet, int size);
void quickSort(int vec[], int left, int right);
void mergeSort(int vec[], int l, int r, int buffer[]);
void heapSort(int arr[], int n);
void radixSort(int arr[], int n);
void bucketSort(int arr[], int n);



const int BUFFER_POOL_SIZE = 1000000;
int bufferPool[BUFFER_POOL_SIZE] = {0};
int bufferPoolIndex = 0;

    
int main() {

    int divisor = 10000;
    while (divisor >= 1) {
        int n = BUFFER_POOL_SIZE / divisor;
        int* array;
        int* table;

        srand((unsigned int)time(NULL));

        array = new int[n];
        table = new int[n];

        for (int i = 0; i < n; i++) {
            table[i] = array[i] = (int)rand() & (int)(pow(2, 16) - 1);
        }


        cout << "N = " << n << ": " << endl;

        // standard sort
        for (int i = 0; i < n; i++) {
            array[i] = table[i];
            bufferPool[i] = 0;
        }
        auto start = std::chrono::high_resolution_clock::now();
        sort(array, array + n);
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        auto std_sort_time = duration.count();
        cout << "   Standard sort took: " << std::setw(8) << std::left << duration.count() << " microseconds (100.0%)" << endl;

        // heap sort
        for (int i = 0; i < n; i++) {
            array[i] = table[i];
            bufferPool[i] = 0;
        }
        start = std::chrono::high_resolution_clock::now();
        heapSort(array, n);
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        cout << "   Heap     sort took: " << std::setw(8) << std::left << duration.count() << " microseconds" << " (" << std::setprecision(4) << (double)duration.count() / std_sort_time * 100 << "%)" << endl;


        // shell sort
        for (int i = 0; i < n; i++) {
            array[i] = table[i];
            bufferPool[i] = 0;
        }
        start = std::chrono::high_resolution_clock::now();
        shellSort(array, n);
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);   
        cout << "   Shell    sort took: " << std::setw(8) << std::left << duration.count() << " microseconds" << " (" << std::setprecision(4) << (double)duration.count() / std_sort_time * 100 << "%)" << endl;


        // merge sort
        for (int i = 0; i < n; i++) {
            array[i] = table[i];
            bufferPool[i] = 0;
        }
        start = std::chrono::high_resolution_clock::now();
        mergeSort(array, 0, n - 1, bufferPool);
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        cout << "   Merge    sort took: " << std::setw(8) << std::left << duration.count() << " microseconds" << " (" << std::setprecision(4) << (double)duration.count() / std_sort_time * 100 << "%)" << endl;


        // quick sort
        for (int i = 0; i < n; i++) {
            array[i] = table[i];
            bufferPool[i] = 0;
        }
        start = std::chrono::high_resolution_clock::now();
        quickSort(array, 0, n - 1);
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        cout << "   Quick    sort took: " << std::setw(8) << std::left << duration.count() << " microseconds" << " (" << std::setprecision(4) << (double)duration.count() / std_sort_time * 100 << "%)" << endl;
        

        // radix sort
        for (int i = 0; i < n; i++) {
            array[i] = table[i];
            bufferPool[i] = 0;
        }
        start = std::chrono::high_resolution_clock::now();
        radixSort(array, n);
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        cout << "   Radix    sort took: " << std::setw(8) << std::left << duration.count() << " microseconds" << " (" << std::setprecision(4) << (double)duration.count() / std_sort_time * 100 << "%)" << endl;

        divisor /= 10;
    }

    system("pause");
    return 0;
}


void radixSort(int arr[], int n)
{
    int maxVal = 0;
    for (int i = 0; i < n; i++)
    {
        if (arr[i] > maxVal)
            maxVal = arr[i];
    }

    int exp = 1;
    int* output = new int[n];

    while (maxVal / exp > 0)
    {
        int count[10] = { 0 };

        for (int i = 0; i < n; i++)
            count[(arr[i] / exp) % 10]++;

        for (int i = 1; i < 10; i++)
            count[i] += count[i - 1];

        for (int i = n - 1; i >= 0; i--)
        {
            output[count[(arr[i] / exp) % 10] - 1] = arr[i];
            count[(arr[i] / exp) % 10]--;
        }

        for (int i = 0; i < n; i++)
            arr[i] = output[i];

        exp *= 10;
    }

    delete[] output;
}


void heapify_BufferPool(int arr[], int n, int i)
{
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < n && arr[l] > arr[largest])
        largest = l;

    if (r < n && arr[r] > arr[largest])
        largest = r;

    if (largest != i)
    {
        swap(arr[i], arr[largest]);
        heapify_BufferPool(arr, n, largest);
    }
    else
    {
        return;
    }
}


void heapSort(int arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify_BufferPool(arr, n, i);

    for (int i = n - 1; i >= 0; i--)
    {
        swap(arr[0], arr[i]);
        heapify_BufferPool(arr, i, 0);
    }
}



void shellSort(int* vet, int size) {
    int i, j, value;
    int gap = 1;
    do {
        gap = 3 * gap + 1;
    } while (gap < size);
    do {
        gap /= 3;
        for (i = gap; i < size; i++) {
            value = vet[i];
            j = i - gap;
            while (j >= 0 && value < vet[j]) {
                vet[j + gap] = vet[j];
                j -= gap;
            }
            vet[j + gap] = value;
        }
    } while (gap > 1);
}


void merge(int vec[], int l, int m, int r, int buffer[]) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;

    for (i = 0; i < n1; i++)
        buffer[i] = vec[l + i];

    for (j = 0; j < n2; j++)
        buffer[n1 + j] = vec[m + 1 + j];

    i = 0;
    j = 0;
    k = l;

    while (i < n1 && j < n2) {
        if (buffer[i] <= buffer[n1 + j]) {
            vec[k] = buffer[i];
            i++;
        }
        else {
            vec[k] = buffer[n1 + j];
            j++;
        }
        k++;
    }

    while (i < n1) {
        vec[k] = buffer[i];
        i++;
        k++;
    }

    while (j < n2) {
        vec[k] = buffer[n1 + j];
        j++;
        k++;
    }
}


void mergeSort(int vec[], int l, int r, int buffer[]) {
    if (l < r) {
        int m = l + (r - l) / 2;
        mergeSort(vec, l, m, buffer);
        mergeSort(vec, m + 1, r, buffer);
        merge(vec, l, m, r, buffer);
    }
}


// function to swap two elements in an array
void swap(int& a, int& b) {
    int temp = a;
    a = b;
    b = temp;
}

// function to return the median of three elements
int medianOfThree(int a, int b, int c) {
    return max(min(a, b), min(max(a, b), c));
}

// function to partition the array around a pivot element
int partition(int arr[], int low, int high) {
    int pivot = medianOfThree(arr[low], arr[high], arr[(low + high) / 2]);
    int i = low - 1;
    int j = high + 1;

    while (true) {
        do {
            i++;
        } while (arr[i] < pivot);

        do {
            j--;
        } while (arr[j] > pivot);

        if (i >= j) {
            return j;
        }

        swap(arr[i], arr[j]);
    }
}

// quicksort function
void quickSort(int arr[], int low, int high) {
    if (low < high) {
        int pivotIndex = partition(arr, low, high);
        quickSort(arr, low, pivotIndex);
        quickSort(arr, pivotIndex + 1, high);
    }
}

